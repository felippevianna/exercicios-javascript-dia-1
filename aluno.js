function calculaMedia(notas) {
    let somaNotas = 0
    for(let nota of notas){
        somaNotas += nota
    }

    let media = somaNotas/3
    if (media >= 6){
        console.log("Aprovado com média "+ media.toFixed(2))
    } else {
        console.log("Reprovado com média " + media.toFixed(2))
    }
}
let nota1 = Math.random()*10
let nota2 = Math.random()*10
let nota3 = Math.random()*10
console.log(`Notas: ${nota1.toFixed(2)}  ${nota2.toFixed(2)}  ${nota3.toFixed(2)}`)
let notas = [nota1, nota2, nota3]
calculaMedia(notas)
